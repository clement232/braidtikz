"""
    braidTikz(braid)

Return the string which can be put in a latex document to make the tikzpicture of the braid.
To get the right string, you have to print the result.

# Arguments
- 'braid::Vector{Int64}': index of the generators of the braid
"""
function braidTikz(braid)
  temp::String = ""
  res::String = ""
  for i in braid
    temp = temp * "s_" * string(i)* " "
  end
  res = raw"\begin{center} \begin{tikzpicture} \pic[rotate=90,braid/.cd,every strand/.style={ultra thick},flip crossing convention] {braid={" * temp * raw"}}; \end{tikzpicture} \end{center}"
  return(res)
end