# braidTikz

## Usage
Use this function to get the latex code for drawing the braid which is given by the index of the generators that compose your braid.